---
title: Separable first-order ODE
author: Damar Wicaksono
date: '2021-03-04'
category:
- lecture-notes
tags:
- maths
---

<script src="/rmarkdown-libs/header-attrs/header-attrs.js"></script>
<link href="/rmarkdown-libs/anchor-sections/anchor-sections.css" rel="stylesheet" />
<script src="/rmarkdown-libs/anchor-sections/anchor-sections.js"></script>


<p>Separable first-order ODE is a class of ODE’s in which analytical solution is possible.
Consider the following ODE:
<span class="math display">\[
g(y) \frac{dy}{dx} = f(x); \;\; y(x_o) = y_o.
\]</span>
In the form above, <span class="math inline">\(g\)</span> and <span class="math inline">\(f\)</span> are solely function of <span class="math inline">\(y\)</span> and <span class="math inline">\(x\)</span>, respectively.
If an ODE can be written in this form then the equation is called <em>separable</em>.</p>
<p>A separable equation can be “easily”<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a> integrated:
<span class="math display">\[
\int_{x_o}^{x}\,g(y(x))\,\frac{dy}{dx}\,dx = \int_{x_o}^{x} f(x)\,dx.
\]</span>
Let <span class="math inline">\(u = y(x)\)</span>, then:
<span class="math display">\[
du = \frac{dy}{dx}\,dx.
\]</span>
Substituting <span class="math inline">\(du\)</span>, gives:
<span class="math display">\[
\int_{u_o}^{u}\,g(u)\,du = \int_{x_o}^{x} f(x)\,dx.
\]</span>
where <span class="math inline">\(u_o = y(x=x_o)\)</span>.</p>
<p>This is the mathematically correct way to integrate the equation. However, as an engineer,
for a long time I thought you can simply rearrange the equation to be:
<span class="math display">\[
\int_{y_o}^{y}\,g(y)\,dy = \int_{x_o}^{x} f(x)\,dx
\]</span>
and indeed apparently it works just as well! The thing is,
in general, <span class="math inline">\(\frac{dy}{dx}\)</span> is not a division in ordinary sense (it’s an operator <span class="math inline">\(\frac{d}{dx}\)</span> on <span class="math inline">\(y\)</span><a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a>)
and one can’t just rearrange the terms.
But in this case, it works because doing the substitution will yield the same expression as “rearranging” the differential terms.</p>
<div id="example-putting-first-order-ode-into-a-separable-form" class="section level2">
<h2>Example: Putting first-order ODE into a separable form</h2>
<p>Put the following equations in a separated form:</p>
<ol style="list-style-type: decimal">
<li><span class="math inline">\(\frac{dy}{dx} = \frac{x^2y - 4y}{x+4}\)</span></li>
<li><span class="math inline">\(\frac{dy}{dx} = \sec{(y)} e^{x-y} (1+x)\)</span></li>
<li><span class="math inline">\(\frac{d \theta}{dt} + \sin{\theta} = 0\)</span></li>
<li><span class="math inline">\(\frac{dy}{dx} = \frac{xy}{(x+1)(y+1)}\)</span></li>
</ol>
<div id="answers" class="section level3">
<h3>Answers</h3>
<ol style="list-style-type: decimal">
<li><span class="math inline">\(\frac{1}{y}\frac{dy}{dx} = -\frac{x^2 + 4}{x+4}\)</span></li>
<li><span class="math inline">\(\cos{(y)}\,e^y\,\frac{dy}{dx} = e^{x} (1+x)\)</span></li>
<li><span class="math inline">\(\csc{(\theta)}\,\frac{d \theta}{dt} = -1\)</span></li>
<li><span class="math inline">\(\frac{y+1}{y}\,\frac{dy}{dx} = \frac{x}{x+1}\)</span></li>
</ol>
</div>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>that would depend on the actual integrands<a href="#fnref1" class="footnote-back">↩︎</a></p></li>
<li id="fn2"><p>and it means <span class="math inline">\(\lim_{\Delta x \to 0} \frac{\Delta y}{\Delta x}\)</span><a href="#fnref2" class="footnote-back">↩︎</a></p></li>
</ol>
</div>
